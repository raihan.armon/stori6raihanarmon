from django.shortcuts import render, redirect
from homepage.models import StatusDB
from homepage.forms import StatusForm

# Create your views here.
def homepage(request):
	if request.method == "POST":
		form = StatusForm(request.POST)
		if form.is_valid():
			data = form.save()
			data.save()
			return redirect('/home/')
	else:
		form = StatusForm()

	objekStatus = StatusDB.objects.order_by('-date')
	return render(request, 'homepage.html', {'form': form, 'status': objekStatus})

def redirecting(request):
	return redirect('/home/')